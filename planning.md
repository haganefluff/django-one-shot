
* [x] Fork and clone the starter project from django-one-shot
* [x] Create a new virtual environment in the repository directory for the project
* [x] Activate the virtual environment
* [x] Upgrade pip
  * [x] `python -m pip install --upgrade pip`
* [x] Install django
  * [x] x
* [x] Install black
  * [x] x
* [x] Install flake8
  * [x] x
* [x] Install djlint
  * [x] `pip install djlint`
* [x] Instal debug toolbar
  * [x] `pip install django-debug-toolbar`

* [x] Deactivate your virtual environment
  * [x] `deactivate`
* [x] Activate your virtual environment
  * [x] `source .venv/bin/activate`
* [x] Use pip freeze to generate a requirements.txt file
  * [x] `pip freeze > requirements.txt`

### Test feature 1

* [x] python manage.py test tests.test_feature_01

tests/test_feature_01.py

---

## Resources

<https://learn-2.galvanize.com/cohorts/3352/blocks/1859/content_files/build/02-django-one-shot/65-django-one-shot-01.md>

## Feature 2

In the repository directory:
* [x] 
* [x] Create a Django project named brain_two so that the manage.py file is in the top directory
  * [x] `django-admin startproject brain_two`
* [x] Install the debug toolbar app in `INSTALLED_APPS`
  * [x] <https://django-debug-toolbar.readthedocs.io/en/latest/installation.html>
Create a Django app named todos and install it in the brain_two Django project in the INSTALLED_APPS list
Run the migrations
Create a super user

## Feature 5
* [x] task	string	maximum length of 100 characters
* [x] due_date	date time	should be optional
* [x] is_completed	boolean	should default to False
* [x] list	foreign key	should be related to the TodoList model and have a related name of "items". It should automatically delete if the to-do list is deleted. (This is a cascade.)

## Feature 7
* [x] This feature is about creating a list view for the TodoList model
* [x] registering the view for a path
* [x] registering the todos paths with the brain_two project
* [x] and creating a template for the view.

You can use either a function view OR a class-based view.

* [x] Create a view that will get all of the instances of the TodoList model and put them in the context for the template.
* [x] Register that view in the todos app for the path "" and the name "todo_list_list" in a new file named todos/urls.py.
* [x] Include the URL patterns from the todos app in the brain_two project with the prefix "todos/".
* [x] Create a template for the list view that complies with the following specifications.

## Feature 7, Template specifications
* [x] The resulting HTML from a request to the path http://localhost:8000/todos/  should result in HTML that has:

* [x] the fundamental five in it
* [x] a main tag that contains:
* [x] div tag that contains:
* [x] an h1 tag with the content "My Lists"
* [x] a table that has two columns:
* [x] the first with the header "Name" and the rows with the names of the Todo lists
* [ ] the second with the header "Number of items" and nothing in those rows because we don't yet have tasks