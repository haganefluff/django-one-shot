from django.urls import path
from todos.views import (
    create_item,
    create_todolist,
    todo_list_view,
    todo_list_detail,
    update_item,
    update_todolist,
    delete_list,
)

urlpatterns = [
    path("", todo_list_view, name="todo_list_list"),
    path("<int:pk>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:pk>/edit/", update_todolist, name="todo_list_update"),
    path("<int:pk>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", create_item, name="todo_item_create"),
    path("items/<int:pk>/edit/", update_item, name="todo_item_update"),
]
