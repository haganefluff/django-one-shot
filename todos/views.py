from django.shortcuts import get_object_or_404, render, redirect
from todos.forms import TodoForm, TodoitemForm
from todos.models import TodoList, TodoItem

# Create your views here.


def todo_list_view(request):
    todolists = TodoList.objects.all()
    context = {"todolist_list": todolists}
    print(todolists)
    return render(request, "todo_lists/my_todo_lists.html", context)


def todo_list_detail(request, pk):
    todolist = TodoList.objects.get(pk=pk)
    context = {"todolist": todolist}
    return render(request, "todo_lists/details.html", context)


def create_todolist(request):
    context = {}
    form = TodoForm(request.POST or None)
    if form.is_valid():
        todolist = form.save()
        return redirect("todo_list_detail", todolist.pk)

    context["form"] = form
    return render(request, "todo_lists/create.html", context)


def update_todolist(request, pk):
    todolist = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.pk)
    else:
        todolist = get_object_or_404(TodoList, pk=pk)
        form = TodoForm(instance=todolist)
    return render(request, "todo_lists/update.html", {"form": form})


def delete_list(request, pk):
    todolist = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todo_lists/delete.html")


def create_item(request):
    if request.method == "POST":
        form = TodoitemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoitemForm()
    return render(request, "todo_items/create.html", {"form": form})


def update_item(request, pk):
    todoitem = get_object_or_404(TodoItem, pk=pk)
    if request.method == "POST":
        form = TodoitemForm(request.POST, instance=todoitem)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoitemForm(instance=todoitem)
    return render(request, "todo_items/update.html", {"form": form})
